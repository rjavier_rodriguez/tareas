<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>User</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<style>
		.container{
			width: 500px;
			height: auto;
			

		}
		table{
			text-align: center;

		}
		thead{
			font-size: 2em;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<article class="container">
		
		<header class="page-heade">
			<h1>Lavarel 4.2<small>Rjavier</small></h1>
		</header>
			
		
	
	
		<div class="panel panel-default">
  		<!-- Default panel contents -->
  		<div class="panel-heading">Javier</div>

  <!-- Table -->
			  <table class="table table-striped">
			    <thead>
							<tr>
								<td>id</td>
								<td>name</td>
								<td>email</td>
							</tr>
						</thead>
						<tbody>
							@foreach($user as $user)
							<tr>
								<td>{{$user->id}}</td>
								<td>{{$user->name}}</td>
								<td>{{$user->email}}</td>
							</tr>
							 @endforeach 
						</tbody>
			  </table>
		</div>
	</article>
</body>
</html>